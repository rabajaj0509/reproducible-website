---
layout: report
year: "2022"
month: "09"
title: "Reproducible Builds in September 2022"
draft: false
date: 2022-10-07 21:25:12
---

[![]({{ "/images/reports/2022-09/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

Welcome to the September 2022 report from the [Reproducible Builds]({{ "/" | relative_url }}) project! In our reports we try to outline the most important things that we have been up to over the past month. As a quick recap, whilst anyone may inspect the source code of free software for malicious flaws, almost all software is distributed to end users as pre-compiled binaries. If you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

---

[![]({{ "/images/reports/2022-09/nsa.png#right" | relative_url }})](https://www.nsa.gov/Press-Room/News-Highlights/Article/Article/3146465/nsa-cisa-odni-release-software-supply-chain-guidance-for-developers/)

David A. Wheeler reported to us that the US National Security Agency ([NSA](https://en.wikipedia.org/wiki/National_Security_Agency)), Cybersecurity and Infrastructure Security Agency ([CISA](https://en.wikipedia.org/wiki/Cybersecurity_and_Infrastructure_Security_Agency)) and the Office of the Director of National Intelligence ([ODNI](https://en.wikipedia.org/wiki/Director_of_National_Intelligence)) have released a document called [*Securing the Software Supply Chain: Recommended Practices Guide for Developers*](https://media.defense.gov/2022/Sep/01/2003068942/-1/-1/0/ESF_SECURING_THE_SOFTWARE_SUPPLY_CHAIN_DEVELOPERS.PDF) (PDF).

As David [remarked in his post to our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2022-September/002684.html), it "*expressly* recommends having reproducible builds as part of 'advanced' recommended mitigations". The publication of this document has been accompanied [by a press release](https://www.nsa.gov/Press-Room/News-Highlights/Article/Article/3146465/nsa-cisa-odni-release-software-supply-chain-guidance-for-developers/).

---

Holger Levsen was made aware of a small Microsoft project called *oss-reproducible*. Part of, [*OSSGadget*](https://github.com/microsoft/OSSGadget), a larger "collection of tools for analyzing open source packages", the purpose of *oss-reproducible* is to:

> analyze open source packages for reproducibility. We start with an existing package (for example, the NPM `left-pad` package, version 1.3.0), and we try to answer the question, **Do the package contents authentically reflect the purported source code?**

More details can be found in the `README.md` file [within the code repository](https://github.com/microsoft/OSSGadget/tree/main/src/oss-reproducible).

---

[![]({{ "/images/reports/2022-09/bestpractice.png#right" | relative_url }})](https://bestpractices.coreinfrastructure.org/en)

David A. Wheeler also pointed out that there are some potential upcoming changes to the [OpenSSF Best Practices](https://bestpractices.coreinfrastructure.org/en) badge for open source software in relation to reproducibility. Whilst the badge programme has [three certification levels](https://bestpractices.coreinfrastructure.org/en/criteria) ("passing", "silver" and "gold"), the "gold" level includes the criterion that "The project MUST have a reproducible build".

[David reported](https://lists.reproducible-builds.org/pipermail/rb-general/2022-September/002696.html) that some projects have argued that this reproducibility criterion should be slightly relaxed as outlined in an [issue on the `best-practices-badge`](https://github.com/coreinfrastructure/best-practices-badge/issues/1865) GitHub project. Essentially, though, the claim is that the reproducibility requirement doesn't make sense for projects that do not release built software, and that timestamp differences by *themselves* don't necessarily indicate malicious changes. Numerous pragmatic problems around excluding timestamps were raised in the discussion of the issue.

---

[![]({{ "/images/reports/2022-09/sonatype.png#right" | relative_url }})](https://www.sonatype.com/press-releases/sonatype-finds-700-average-increase-in-open-source-supply-chain-attacks)

[Sonatype](https://www.sonatype.com/), a "pioneer of software supply chain management", issued a [press release](https://www.sonatype.com/press-releases/sonatype-finds-700-average-increase-in-open-source-supply-chain-attacks) month to report that they had found:

> […] a massive year-over-year increase in cyberattacks aimed at open source project ecosystems. According to early data from Sonatype's 8th annual State of the Software Supply Chain Report, which will be released in full this October, Sonatype has recorded an average 700% jump in repository attacks over the last three years.

More information is available [in the press release](https://www.sonatype.com/press-releases/sonatype-finds-700-average-increase-in-open-source-supply-chain-attacks).

---

[![]({{ "/images/reports/2022-09/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

A number of changes were made to the Reproducible Builds website and documentation this month, including Chris Lamb adding a redirect from [`/projects/`]({{ "/projects/" | relative_url }}) to [`/who/`]({{ "/who/" | relative_url }}) in order to keep old or archived links working&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/5926d0df)], Jelle van der Waa added a [Rust](https://www.rust-lang.org/) programming language [example for `SOURCE_DATE_EPOCH`]({{ "/docs/source-date-epoch/#rust" | relative_url }})&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/ea2f4306)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/561d91de)] and Mattia Rizzolo included [Protocol Labs](https://protocol.ai/) amongst our [project-level sponsors]({{ "/who/sponsors/" | relative_url }})&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/e39c8a8e)].

<br>

### Debian

[![]({{ "/images/reports/2022-09/debian.png#right" | relative_url }})](https://debian.org/)

There was a large amount of reproducibility work taking place within [Debian](https://debian.org/) this month:

* The `nfft` source package was removed from the archive, and now *all* packages in Debian *bookworm* now have a corresponding `.buildinfo` file. This can be confirmed and tracked on the [associated page on the](https://tests.reproducible-builds.org/debian/bookworm/amd64/index_no_buildinfos.html) *tests.reproducible-builds.org* site.

* Vagrant Cascadian [announced on our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2022-September/002689.html) an informal online sprint to help "clear the huge backlog of reproducible builds patches submitted" by performing NMU ([Non-Maintainer Uploads](https://wiki.debian.org/NonMaintainerUpload)). The first such sprint took place on September 22nd with the following results:

    * Holger Levsen:

        - Mailed [#1010957](https://bugs.debian.org/1010957) in `man-db` asking for an update and whether to remove the patch tag for now. This was subsequently removed and the maintainer started to address the issue.
        - Uploaded `gmp` to `DELAYED/15`, fixing [#1009931](https://bugs.debian.org/1009931).
        - Emailed [#1017372](https://bugs.debian.org/1017372) in `plymouth` and asked for the maintainer's opinion on the patch. This resulted in the maintainer improving Vagrant's original patch (and uploading it) as well as [filing an issue upstream](https://gitlab.freedesktop.org/plymouth/plymouth/-/issues/188).
        - Uploaded `time` to `DELAYED/15`, fixing [#983202](https://bugs.debian.org/983202).

    * Vagrant Cascadian:

        - Verify and updated patch for `mylvmbackup` ([#782318](https://bugs.debian.org/782318))
        - Verified/updated patches for `libranlip`. ([#788000](https://bugs.debian.org/788000), [#846975](https://bugs.debian.org/846975) & [#1007137](https://bugs.debian.org/1007137))
        - Uploaded `libranlip` to `DELAYED/10`.
        - Verified patch for `cclive`. ([#824501](https://bugs.debian.org/824501))
        - Uploaded `cclive` to `DELAYED/10`.
        - Vagrant was unable to reproduce the underlying issue within [#791423](https://bugs.debian.org/791423) (`linuxtv-dvb-apps`) and so the bug was marked as "done".
        - Researched [#794398](https://bugs.debian.org/794398) (in `clhep`).

    The plan is to repeat these sprints every two weeks, with the next taking place on [**Thursday October 6th at 16:00 UTC**](https://time.is/compare/1600_06_Oct_2022_in_UTC) on the `#debian-reproducible` IRC channel.

* Roland Clobus posted his [13th update of the status of reproducible Debian ISO images](https://lists.reproducible-builds.org/pipermail/rb-general/2022-September/002693.html) on our mailing list. During the last month, Roland ensured that the live images are now automatically fed to [openQA](https://openqa.debian.net) for automated testing after they have been shown to be reproducible. Additionally Roland asked on the debian-devel mailing list about a way to determine the canonical timestamp of the Debian archive. [[...](https://lists.debian.org/debian-devel/2022/09/msg00199.html)]

* Following up on [last month's work on reproducible bootstrapping]({{ "/reports/2022-08/" | relative_url }}), Holger Levsen filed two bugs against the *debootstrap* and *cdebootstrap* utilities. ([#1019697](https://bugs.debian.org/1019697) & [#1019698](https://bugs.debian.org/1019698))

Lastly, 44 reviews of Debian packages were added, 91 were updated and 17 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A number of issue types have been updated too, including the descriptions of `cmake_rpath_contains_build_path` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/6c4d7438)], `nondeterministic_version_generated_by_python_param` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/d6d81ff0)] and `timestamps_in_documentation_generated_by_org_mode` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/fcb9f175)]. Furthermore, two new issue types were created: `build_path_used_to_determine_version_or_package_name` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/4259239c)] and `captures_build_path_via_cmake_variables` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/a687dc93)].

### Other distributions

In openSUSE, Bernhard M. Wiedemann published his usual [openSUSE monthly report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/XQQ44R5MZ2HUKYXDZXUOEMTNLRFQVFBJ/).

### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-09/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions `222` and `223` to Debian, as well as made the following changes:

* The `cbfstools` utility is now provided in Debian via the `coreboot-utils` package so we can enable that functionality within Debian. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e21f4153)]

* Looked into [Mach-O support](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/313).

* Fixed the [*try.diffoscope.org*](https://try.diffoscope.org/) service by addressing a compatibility issue between `glibc`/`seccomp` that was preventing the Docker-contained *diffoscope* instance from spawning any external processes whatsoever [[...](https://salsa.debian.org/reproducible-builds/try.diffoscope.org/commit/3fa5eb9)]. I also updated the `requirements.txt` file, as some of the specified packages were no longer available [[...](https://salsa.debian.org/reproducible-builds/try.diffoscope.org/commit/4f3f3a7)][[...](https://salsa.debian.org/reproducible-builds/try.diffoscope.org/commit/dec1878)].

In addition Jelle van der Waa added support for `file` version 5.43&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/88c08e7e)] and Mattia Rizzolo updated the packaging:

* Also include `coreboot-utils` in the `Build-Depends` and `Test-Depends` fields so that it is available for tests.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5ac8ede5)]
* Use `pep517 and pip to load the requirements.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2f2d803f)]
* Remove packages in `Breaks`/`Replaces` that have been obsoleted since the release of Debian *bullseye*.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/40cd80fc)]

### [*Reprotest*](https://tracker.debian.org/pkg/reprotest)

[*reprotest*](https://tracker.debian.org/pkg/reprotest) is our end-user tool to build the same source code twice in widely and deliberate different environments, and checking whether the binaries produced by the builds have any differences. This month, *reprotest* version `0.7.22` was [uploaded to Debian unstable](https://tracker.debian.org/news/1360152/accepted-reprotest-0722-source-into-unstable/) by Holger Levsen, which included the following changes by Philip Hands:

* Actually ensure that the `setarch(8)` utility can actually execute before including an architecture to test.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/aa9a790)]
* Include all files matching `*.*deb` in the default `artifact_pattern` in order to archive all results of the build.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/b2cfd30)]
* Emit an error when building the Debian package if the Debian packaging version does not patch the "Python" version of *reprotest*.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/bfa0eca)]
* Remove an unneeded invocation of the `head(1)` utility.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/48b9c11)]

### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann (18 bugs):

    * [`DateTime`](https://github.com/zopefoundation/DateTime/issues/41) (fails to build in 2038)
    * [`FreeRCT`](https://github.com/FreeRCT/FreeRCT/pull/303) (date-related issue)
    * [`clanlib1`](https://build.opensuse.org/request/show/1005798) (filesystem ordering)
    * [`cli`](https://github.com/cli/cli/issues/6259) (fails to build in 2038)
    * [`deepin-gettext-tools`](https://build.opensuse.org/request/show/1002975) (patch+version update toolchain sort python glob)
    * [`mariadb`](https://bugzilla.opensuse.org/show_bug.cgi?id=1203310) (fails to build in 2038)
    * [`mercurial`](https://bugzilla.opensuse.org/show_bug.cgi?id=1203317) (fails to build in 2038)
    * [`mirrormagic`](https://build.opensuse.org/request/show/1005891) (parallelism-related issue)
    * [`ocaml-extlib`](https://build.opensuse.org/request/show/1005916) (parallelism-related issue)
    * [`python-xmlrpc/python-softlayer`](https://bugzilla.opensuse.org/show_bug.cgi?id=1203311) (fails to build in 2038)
    * [`python`](https://build.opensuse.org/request/show/1003076) (fails to build in 2038)
    * [`q3rally`](https://build.opensuse.org/request/show/1005783) (zip-related issue)
    * [`rnd_jue`](https://build.opensuse.org/request/show/1005890) (parallelism-related issue)
    * [`rsync`](https://build.opensuse.org/request/show/1002273) (workaround an issue in GCC 7.x)
    * [`scons`](https://github.com/SCons/scons/pull/4239) (`SOURCE_DATE_EPOCH`-related issue)
    * [`stratagus`](https://github.com/Wargus/stratagus/pull/415) (date-related issue)
    * [`triplane`](https://github.com/vranki/triplane/pull/5) (nondeterminism caused by uninitialised memory)
    * [`tyrquake`](https://build.opensuse.org/request/show/1005912) (date-related issue)

* Chris Lamb:

    * [#1019382](https://bugs.debian.org/1019382) filed against [`gnome-online-accounts`](https://tracker.debian.org/pkg/gnome-online-accounts).
    * There was renewed activity on a reproducibility-related bug in the [Sphinx](https://www.sphinx-doc.org/) documentation tool this month. Originally filed in October 2021 by Chris Lamb, the bug in question relates to [contents of the `LANGUAGE` environment variable inconsistently affecting the output of `objects.inv` files](https://github.com/sphinx-doc/sphinx/issues/9778).

* Jelle van der Waa:

    * [`mp4v2`](https://github.com/enzo1982/mp4v2/pull/17) (date-related issue)
    * [`mm-common`](https://gitlab.gnome.org/GNOME/mm-common/-/merge_requests/6) (uid/gid issue)
    * [`aardvark-dns`](https://github.com/containers/aardvark-dns/pull/229) (date-related issue)

* Vagrant Cascadian (70 bugs!):

    * [#1020648](https://bugs.debian.org/1020648) filed against [`extrepo-data`](https://tracker.debian.org/pkg/extrepo-data).
    * [#1020650](https://bugs.debian.org/1020650) filed against [`tmpreaper`](https://tracker.debian.org/pkg/tmpreaper).
    * [#1020651](https://bugs.debian.org/1020651) filed against [`xmlrpc-epi`](https://tracker.debian.org/pkg/xmlrpc-epi).
    * [#1020653](https://bugs.debian.org/1020653) filed against [`pal`](https://tracker.debian.org/pkg/pal).
    * [#1020656](https://bugs.debian.org/1020656) filed against [`nvram-wakeup`](https://tracker.debian.org/pkg/nvram-wakeup).
    * [#1020657](https://bugs.debian.org/1020657) filed against [`netris`](https://tracker.debian.org/pkg/netris).
    * [#1020658](https://bugs.debian.org/1020658) filed against [`netpbm-free`](https://tracker.debian.org/pkg/netpbm-free).
    * [#1020659](https://bugs.debian.org/1020659) filed against [`lookup`](https://tracker.debian.org/pkg/lookup).
    * [#1020660](https://bugs.debian.org/1020660) filed against [`logtools`](https://tracker.debian.org/pkg/logtools).
    * [#1020661](https://bugs.debian.org/1020661) filed against [`libid3tag`](https://tracker.debian.org/pkg/libid3tag).
    * [#1020662](https://bugs.debian.org/1020662) filed against [`log4cpp`](https://tracker.debian.org/pkg/log4cpp).
    * [#1020665](https://bugs.debian.org/1020665) filed against [`libimage-imlib2-perl`](https://tracker.debian.org/pkg/libimage-imlib2-perl).
    * [#1020668](https://bugs.debian.org/1020668) filed against [`jnettop`](https://tracker.debian.org/pkg/jnettop).
    * [#1020670](https://bugs.debian.org/1020670) filed against [`gwaei`](https://tracker.debian.org/pkg/gwaei).
    * [#1020671](https://bugs.debian.org/1020671) filed against [`ipfm`](https://tracker.debian.org/pkg/ipfm).
    * [#1020672](https://bugs.debian.org/1020672) filed against [`tarlz`](https://tracker.debian.org/pkg/tarlz).
    * [#1020673](https://bugs.debian.org/1020673) filed against [`w3cam`](https://tracker.debian.org/pkg/w3cam).
    * [#1020674](https://bugs.debian.org/1020674) filed against [`ifstat`](https://tracker.debian.org/pkg/ifstat).
    * [#1020715](https://bugs.debian.org/1020715) filed against [`xserver-xorg-input-joystick`](https://tracker.debian.org/pkg/xserver-xorg-input-joystick).
    * [#1020719](https://bugs.debian.org/1020719) filed against [`chibicc`](https://tracker.debian.org/pkg/chibicc).
    * [#1020723](https://bugs.debian.org/1020723) filed against [`python-omegaconf`](https://tracker.debian.org/pkg/python-omegaconf).
    * [#1020724](https://bugs.debian.org/1020724) and [#1020725](https://bugs.debian.org/1020725) filed against [`snapper`](https://tracker.debian.org/pkg/snapper).
    * [#1020736](https://bugs.debian.org/1020736) filed against [`libreswan`](https://tracker.debian.org/pkg/libreswan).
    * [#1020743](https://bugs.debian.org/1020743) filed against [`pure-ftpd`](https://tracker.debian.org/pkg/pure-ftpd).
    * [#1020748](https://bugs.debian.org/1020748) filed against [`xcolmix`](https://tracker.debian.org/pkg/xcolmix).
    * [#1020749](https://bugs.debian.org/1020749) filed against [`gigalomania`](https://tracker.debian.org/pkg/gigalomania).
    * [#1020750](https://bugs.debian.org/1020750) filed against [`xjump`](https://tracker.debian.org/pkg/xjump).
    * [#1020751](https://bugs.debian.org/1020751) filed against [`waili`](https://tracker.debian.org/pkg/waili).
    * [#1020752](https://bugs.debian.org/1020752) filed against [`sjeng`](https://tracker.debian.org/pkg/sjeng).
    * [#1020753](https://bugs.debian.org/1020753) filed against [`seqtk`](https://tracker.debian.org/pkg/seqtk).
    * [#1020754](https://bugs.debian.org/1020754) filed against [`shapetools`](https://tracker.debian.org/pkg/shapetools).
    * [#1020755](https://bugs.debian.org/1020755) filed against [`rotter`](https://tracker.debian.org/pkg/rotter).
    * [#1020756](https://bugs.debian.org/1020756) filed against [`rakarrack`](https://tracker.debian.org/pkg/rakarrack).
    * [#1020757](https://bugs.debian.org/1020757) filed against [`rig`](https://tracker.debian.org/pkg/rig).
    * [#1020759](https://bugs.debian.org/1020759) filed against [`postal`](https://tracker.debian.org/pkg/postal).
    * [#1020798](https://bugs.debian.org/1020798) filed against [`netkit-rsh`](https://tracker.debian.org/pkg/netkit-rsh).
    * [#1020800](https://bugs.debian.org/1020800) filed against [`libapache-mod-evasive`](https://tracker.debian.org/pkg/libapache-mod-evasive).
    * [#1020804](https://bugs.debian.org/1020804) filed against [`paxctl`](https://tracker.debian.org/pkg/paxctl).
    * [#1020805](https://bugs.debian.org/1020805) filed against [`png23d`](https://tracker.debian.org/pkg/png23d).
    * [#1020806](https://bugs.debian.org/1020806) filed against [`perl-byacc`](https://tracker.debian.org/pkg/perl-byacc).
    * [#1020807](https://bugs.debian.org/1020807) filed against [`poster`](https://tracker.debian.org/pkg/poster).
    * [#1020808](https://bugs.debian.org/1020808) filed against [`powerdebug`](https://tracker.debian.org/pkg/powerdebug).
    * [#1020809](https://bugs.debian.org/1020809) filed against [`aespipe`](https://tracker.debian.org/pkg/aespipe).
    * [#1020810](https://bugs.debian.org/1020810) filed against [`aewm++-goodies`](https://tracker.debian.org/pkg/aewm++-goodies).
    * [#1020811](https://bugs.debian.org/1020811) filed against [`apache-upload-progress-module`](https://tracker.debian.org/pkg/apache-upload-progress-module).
    * [#1020812](https://bugs.debian.org/1020812) filed against [`ascii2binary`](https://tracker.debian.org/pkg/ascii2binary).
    * [#1020813](https://bugs.debian.org/1020813) filed against [`bible-kjv`](https://tracker.debian.org/pkg/bible-kjv).
    * [#1020814](https://bugs.debian.org/1020814) filed against [`dradio`](https://tracker.debian.org/pkg/dradio).
    * [#1020815](https://bugs.debian.org/1020815) filed against [`libapache2-mod-python`](https://tracker.debian.org/pkg/libapache2-mod-python).
    * [#1020816](https://bugs.debian.org/1020816) filed against [`tempest-for-eliza`](https://tracker.debian.org/pkg/tempest-for-eliza).
    * [#1020817](https://bugs.debian.org/1020817) filed against [`aplus-fsf`](https://tracker.debian.org/pkg/aplus-fsf).
    * [#1020866](https://bugs.debian.org/1020866) filed against [`wrapsrv`](https://tracker.debian.org/pkg/wrapsrv).
    * [#1020867](https://bugs.debian.org/1020867) filed against [`uclibc`](https://tracker.debian.org/pkg/uclibc).
    * [#1020870](https://bugs.debian.org/1020870) filed against [`xppaut`](https://tracker.debian.org/pkg/xppaut).
    * [#1020872](https://bugs.debian.org/1020872) filed against [`xvier`](https://tracker.debian.org/pkg/xvier).
    * [#1020873](https://bugs.debian.org/1020873) filed against [`xserver-xorg-video-glide`](https://tracker.debian.org/pkg/xserver-xorg-video-glide).
    * [#1020875](https://bugs.debian.org/1020875) filed against [`z80asm`](https://tracker.debian.org/pkg/z80asm).
    * [#1020876](https://bugs.debian.org/1020876) filed against [`yaskkserv`](https://tracker.debian.org/pkg/yaskkserv).
    * [#1020877](https://bugs.debian.org/1020877) filed against [`edid-decode`](https://tracker.debian.org/pkg/edid-decode).
    * [#1020878](https://bugs.debian.org/1020878) filed against [`dustmite`](https://tracker.debian.org/pkg/dustmite).
    * [#1020879](https://bugs.debian.org/1020879) filed against [`dustmite`](https://tracker.debian.org/pkg/dustmite).
    * [#1020880](https://bugs.debian.org/1020880) filed against [`libapache2-mod-authnz-pam`](https://tracker.debian.org/pkg/libapache2-mod-authnz-pam).
    * [#1020881](https://bugs.debian.org/1020881) filed against [`kafs-client`](https://tracker.debian.org/pkg/kafs-client).
    * [#1020882](https://bugs.debian.org/1020882) filed against [`yaku-ns`](https://tracker.debian.org/pkg/yaku-ns).
    * [#1020884](https://bugs.debian.org/1020884) filed against [`bplay`](https://tracker.debian.org/pkg/bplay).
    * [#1020886](https://bugs.debian.org/1020886) filed against [`chise-base`](https://tracker.debian.org/pkg/chise-base).
    * [#1020887](https://bugs.debian.org/1020887) filed against [`checkpw`](https://tracker.debian.org/pkg/checkpw).
    * [#1020888](https://bugs.debian.org/1020888) filed against [`clamz`](https://tracker.debian.org/pkg/clamz).
    * [#1020889](https://bugs.debian.org/1020889) filed against [`libapache2-mod-auth-pgsql`](https://tracker.debian.org/pkg/libapache2-mod-auth-pgsql).

### Testing framework

[![]({{ "/images/reports/2022-09/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a significant testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org) in order to check packages and other artifacts for reproducibility. This month, however, the following changes were made:

* Holger Levsen:

    * Add a job to build *reprotest* from Git&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cae18bea)] and use the correct Git branch when building it&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/012fb28e)].

* Mattia Rizzolo:

    * Enable syncing of results from building live Debian ISO images.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9e3d80df)]
    * Use `scp -p` in order to preserve modification times when syncing live ISO images.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/43d61a2f)]
    * Apply the [shellcheck](https://www.shellcheck.net/) shell script analysis tool.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/17547cc6)]
    * In a build node wrapper script, remove some debugging code which was messing up calling `scp(1)` correctly&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/41fd4fd9)] and consquently add support to use both `scp -p` and regular `scp`&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ee995b43)].

* Roland Clobus:

    * Track and handle the case where the Debian archive gets updated between two live image builds.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ff1efeec)]
    * Remove a call to `sudo(1)` as it is not (or no longer) required to delete old *live-build* results.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e79f4ae4)]

### Contact

As ever, if you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)

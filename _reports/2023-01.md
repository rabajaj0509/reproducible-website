---
layout: report
year: "2023"
month: "01"
title: "Reproducible Builds in January 2023"
draft: false
date: 2023-02-06 00:37:36
---

**Welcome to the first report for 2023 from the [Reproducible Builds](https://reproducible-builds.org) project!**
{: .lead}

[![]({{ "/images/reports/2023-01/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

In these reports we try and outline the most important things that we have been up to over the past month, as well as the most important things in/around the community. As a quick recap, the motivation behind the reproducible builds effort is to ensure no malicious flaws can be deliberately introduced during compilation and distribution of the software that we run on our devices. As ever, if you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

---

## News

[![]({{ "/images/reports/2023-01/github.png#right" | relative_url }})](https://github.blog/changelog/2023-01-30-git-archive-checksums-may-change/)

In a curious turn of events, GitHub first announced this month that the checksums of various Git archives may be subject to change, specifically that because:

> … the default compression for Git archives has recently changed. As result, archives downloaded from GitHub may have different checksums even though the contents are completely unchanged.

This change (which [was brought up on our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2022-October/002709.html) last October) would have had quite wide-ranging implications for anyone wishing to validate and verify downloaded archives using cryptographic signatures. However, GitHub reversed this decision, updating their [original announcement](https://github.blog/changelog/2023-01-30-git-archive-checksums-may-change/) with a message that "We are reverting this change for now. More details to follow." It appears that this was informed in part by an [in-depth discussion in the GitHub Community issue tracker](https://github.com/orgs/community/discussions/45830).

<br>

[![]({{ "/images/reports/2023-01/bsi.png#right" | relative_url }})](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Lageberichte/Lagebericht2022.pdf?__blob=publicationFile&v=6)

The *Bundesamt für Sicherheit in der Informationstechnik* (BSI) (trans: 'The Federal Office for Information Security') is the agency in charge of managing computer and communication security for the German federal government. They recently produced a report that touches on attacks on software supply-chains (*Supply-Chain-Angriff*). ([German PDF](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Lageberichte/Lagebericht2022.pdf?__blob=publicationFile&v=6))

<br>

[![]({{ "/images/reports/2023-01/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

Contributor *Seb35* updated [our website]({{ "/" | relative_url }}) to fix broken links to [Tails](https://tails.boum.org/)' Git repository [[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/7f98d24b)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/f3ae46cc)], and Holger updated a large number of pages around our [recent summit in Venice]({{ "/events/venice2022/" | relative_url }}) [[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/80e4e5b9)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/af639729)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/78a0a627)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/385c83e9)].

<br>

[![]({{ "/images/reports/2023-01/jonsson-paper.png#right" | relative_url }})](https://www.cesarsotovalero.net/files/publications/The_State_Of_Software_Diversity_In_The_Software_Supply_Chain.pdf)

Noak Jönsson has written an interesting paper entitled [*The State of Software Diversity in the Software Supply Chain of Ethereum Clients*](https://www.cesarsotovalero.net/files/publications/The_State_Of_Software_Diversity_In_The_Software_Supply_Chain.pdf). As the paper outlines:

> In this report, the software supply chains of the most popular Ethereum clients are cataloged and analyzed. The dependency graphs of Ethereum clients developed in Go, Rust, and Java, are studied. These client are Geth, Prysm, OpenEthereum, Lighthouse, Besu, and Teku. To do so, their dependency graphs are transformed into a unified format. Quantitative metrics are used to depict the software supply chain of the blockchain. The results show a clear difference in the size of the software supply chain required for the execution layer and consensus layer of Ethereum.

<br>

Yongkui Han posted to [our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/) discussing [making reproducible builds & GitBOM work together without gitBOM-ID embedding](https://lists.reproducible-builds.org/pipermail/rb-general/2023-January/002813.html). GitBOM (now renamed to [OmniBOR](https://omnibor.io/)) is a project to "enable automatic, verifiable artifact resolution across today's diverse software supply-chains" [[…](https://omnibor.io/)]. In addition, Fabian Keil wrote to us asking whether [anyone in the community would be at Chemnitz Linux Days 2023](https://lists.reproducible-builds.org/pipermail/rb-general/2023-January/002811.html), which is due to take place on 11th and 12th March ([event info](https://chemnitzer.linux-tage.de/2023/en)).

[![]({{ "/images/reports/2023-01/fosdem.jpeg#right" | relative_url }})](https://fosdem.org/)

Separate to this, Akihiro Suda posted to our mailing list just after the end of the month with a [status report of bit-for-bit reproducible Docker/OCI images](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002842.html). As Akihiro mentions in their post, they will be giving a talk at [FOSDEM](https://fosdem.org/) in the ['Containers' devroom](https://fosdem.org/2023/schedule/track/containers/) titled [*Bit-for-bit reproducible builds with `Dockerfile`*](https://fosdem.org/2023/schedule/event/container_reproducible_dockerfile/) and that "my talk will also mention how to pin the apt/dnf/apk/pacman packages with my [`repro-get`](https://github.com/reproducible-containers/repro-get) tool."

<br>

[![]({{ "/images/reports/2023-01/signal.png#right" | relative_url }})](https://signal.org/)

The extremely popular [Signal messenger app](https://signal.org/) added upstream support for the [`SOURCE_DATE_EPOCH`]({{ "/docs/source-date-epoch/" | relative_url }}) environment variable this month. This means that release tarballs of the Signal desktop client do not embed nondeterministic release information. [[…](https://github.com/signalapp/Signal-Desktop/pull/5753)][[…](https://github.com/signalapp/Signal-Desktop/pull/6212)]

<br>

---

## Distribution work

### F-Droid & Android

[![]({{ "/images/reports/2023-01/fdroid.png#right" | relative_url }})](https://f-droid.org/)

There was a very large number of changes in the [F-Droid](https://f-droid.org/) and wider Android ecosystem this month:

On January 15th, a blog post entitled [*Towards a reproducible F-Droid*](https://f-droid.org/en/2023/01/15/towards-a-reproducible-fdroid.html) was published on the [F-Droid website](https://f-droid.org/), outlining the reasons why "F-Droid signs published APKs with its own keys" and how reproducible builds allow using upstream developers' keys instead. In particular:

> In response to […] criticisms, we started [encouraging new apps to enable reproducible builds](https://gitlab.com/fdroid/fdroiddata/-/issues/2816). It turns out that reproducible builds are not so difficult to achieve for many apps. [In the past few months we’ve gotten many more reproducible apps in F-Droid than before](https://gitlab.com/fdroid/fdroiddata/-/issues/2844). Currently we can’t highlight which apps are reproducible in the client, so maybe you haven’t noticed that there are many new apps signed with upstream developers’ keys.

(There was a [discussion about this post](https://news.ycombinator.com/item?id=34457143) on [Hacker News](https://news.ycombinator.com/).)

In addition:

* F-Droid added 13 apps published with reproducible builds this month. [[…](https://gitlab.com/fdroid/fdroiddata/-/issues/2844)]

* FC Stegerman outlined a bug where [`baseline.profm` files are nondeterministic](https://gist.github.com/obfusk/61046e09cee352ae6dd109911534b12e), developed a workaround, and provided all the details required for a fix. As they note, this issue [has now been fixed](https://android.googlesource.com/platform/tools/base/+/2f2c6b30b55e18e2672edf5ee8e8e583be759d3e) but the fix is not yet part of an official [Android Gradle plugin release](https://developer.android.com/studio/releases/gradle-plugin).

* GitLab user [*Parwor*](https://gitlab.com/Parwor) discovered that the number of CPU cores can affect the reproducibility of `.dex` files.&nbsp;[[…](https://gitlab.com/fdroid/rfp/-/issues/1519#note_1226216164)]

* FC Stegerman also announced the `0.2.0` and `0.2.1` releases of [*reproducible-apk-tools*](https://github.com/obfusk/reproducible-apk-tools), a suite of tools to help make `.apk` files reproducible. Several new subcommands and scripts were added, and a number of bugs were fixed as well&nbsp;[[…](https://lists.reproducible-builds.org/pipermail/rb-general/2023-January/002815.html)][[…](https://lists.reproducible-builds.org/pipermail/rb-general/2023-January/002816.html)]. They also updated the [F-Droid website](https://f-droid.org/) to improve the reproducibility-related documentation.&nbsp;[[…](https://gitlab.com/fdroid/fdroid-website/-/merge_requests/895/diffs)][[…](https://gitlab.com/fdroid/fdroid-website/-/merge_requests/901/diffs)]

* On the F-Droid issue tracker, FC Stegerman [discussed reproducible builds](https://gitlab.com/fdroid/fdroiddata/-/merge_requests/12145#note_1231715091) with one of the developers of the [Threema messenger app](https://threema.ch/) and reported that Android SDK *build-tools* `31.0.0` and `32.0.0` (unlike earlier and later versions) have a [`zipalign` command that produces incorrect padding](https://gitlab.com/fdroid/fdroiddata/-/issues/2816#note_1249683547).

* A number of bugs related to reproducibility were discovered in Android itself. Firstly, the non-deterministic order of `.zip` entries in `.apk` files [[…](https://issuetracker.google.com/issues/265653160)] and then newline differences between building on Windows versus Linux that can make builds not reproducible as well.&nbsp;[[…](https://issuetracker.google.com/issues/266109851)] (Note that these links may require a Google account to view.)

* And just before the end of the month, FC Stegerman started a thread on [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general) on the topic of [hiding data/code in APK embedded signatures](https://lists.reproducible-builds.org/pipermail/rb-general/2023-January/002825.html) which has been made possible by the [Android APK Signature Scheme v2/v3](https://source.android.com/docs/security/features/apksigning). As part of this, they made an Android app that reads the APK Signing block of its own APK and extracts a payload in order to alter its behaviour called [*sigblock-code-poc*](https://github.com/obfusk/sigblock-code-poc).

### Debian

[![]({{ "/images/reports/2023-01/debian.png#right" | relative_url }})](https://debian.org/)

As mentioned in [last month's report]({{ "/reports/2022-12/" | relative_url }}), Vagrant Cascadian has been organising a series of online sprints in order to 'clear the huge backlog of reproducible builds patches submitted' by performing NMUs ([Non-Maintainer Uploads](https://wiki.debian.org/NonMaintainerUpload)). During January, a sprint took place on the 10th, resulting in the following uploads:

* Chris Lamb:

    * [`critcl`](https://tracker.debian.org/pkg/critcl) ([#963600](https://bugs.debian.org/963600))
    * [`log4cpp`](https://tracker.debian.org/pkg/log4cpp) ([#1020662](https://bugs.debian.org/1020662))
    * [`logapp`](https://tracker.debian.org/pkg/logapp) ([#1010845](https://bugs.debian.org/1010845))
    * [`nanomsg`](https://tracker.debian.org/pkg/nanomsg) ([#1001853](https://bugs.debian.org/1001853))

* Holger Levsen:

    * [`netkit-rsh`](https://tracker.debian.org/pkg/netkit-rsh) ([#1020798](https://bugs.debian.org/1020798))
    * [`wcwidth`](https://tracker.debian.org/pkg/wcwidth) ([#1005408](https://bugs.debian.org/1005408))

* Vagrant Cascadian:

    * [`mc`](https://tracker.debian.org/pkg/mc) ([#828683](https://bugs.debian.org/828683))
    * [`gtk-sharp3`](https://tracker.debian.org/pkg/gtk-sharp3) ([#989965](https://bugs.debian.org/989965) & [#989966](https://bugs.debian.org/989966))

During this sprint, Holger Levsen filed Debian bug [#1028615](https://bugs.debian.org/1028615) to request that the [`tracker.debian.org`](https://tracker.debian.org) service display results of reproducible rebuilds, not just reproducible 'CI' results.

Elsewhere in Debian, [*strip-nondeterminism*](https://tracker.debian.org/pkg/strip-nondeterminism) is our tool to remove specific non-deterministic results from a completed build. This month, version `1.13.1-1` was [uploaded to Debian unstable](https://tracker.debian.org/news/1409573/accepted-strip-nondeterminism-1131-1-source-into-unstable/) by Holger Levsen, including a fix by FC Stegerman (*obfusk*) to update a regular expression for the latest version of `file(1)`&nbsp;[[…](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/f1017c6)].&nbsp;([#1028892](https://bugs.debian.org/1028892))

Lastly, 65 reviews of Debian packages were added, 21 were updated and 35 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html).

### Other distributions

In other distributions:

[![]({{ "/images/reports/2023-01/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

* Bernhard M. Wiedemann published another [monthly report for reproducibility within openSUSE](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/RQ3TMTIOU7HUX5TIP7IE7KT7ZWERWPXB/), as well as [a belated report for December 2022](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/2DKRGC4EIBVUVP6RWHBCEL5SJKCTWRFM/).

* It was announced that [Fedora Rawhide](https://docs.fedoraproject.org/en-US/releases/rawhide/) now 'clamps'  file modification types to [`SOURCE_DATE_EPOCH`]({{ "/docs/source-date-epoch/" | relative_url }}).&nbsp;[[…](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/message/2BDU7CFZGEXUWBHJIZDB2QAOIR2R5TFN/)]

* Finally, an existing tool called [*rpmreproduce*](https://github.com/fepitre/rpmreproduce) was (re-)discovered this month, which claims that "given a buildinfo file from a RPM package, [it can] generate instructions for attempting to reproduce the binary packages built from the associated source and build information."

---

## [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2023-01/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb made the following changes to [diffoscope](https://diffoscope.org), including preparing and uploading versions `231`, `232`, `233` and `234` to Debian:

* No need for `from __future__ import print_function` import anymore. [[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7a1dc409)]
* Comment and tidy the `extras_require.json` handling. [[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7129af15)]
* Split inline Python code to generate test `Recommends` into a separate Python script. [[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c341b63a)]
* Update `debian/tests/control` after merging support for [PyPDF](https://pypdf2.readthedocs.io/) support. [[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2e317927)]
* Correctly catch segfaulting `cd-iccdump` binary. [[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2d95ae41)]
* Drop some old debugging code. [[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/fca2293a)]
* Allow ICC tests to (temporarily) fail. [[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/dff253b5)]

In addition, FC Stegerman (*obfusk*) made a number of changes, including:

* Updating the `test_text_proper_indentation` test to support the latest version(s) of [`file(1)`](https://en.wikipedia.org/wiki/File_(command)). [[…](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/329)]
* Use an `extras_require.json` file to store some build/release metadata, instead of accessing the internet. [[…](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/325)]
* Updating an APK-related [`file(1)`](https://en.wikipedia.org/wiki/File_(command)) regular expression.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/72f8f33d)]
* On the [*diffoscope.org* website](https://diffoscope.org/), de-duplicate contributors by e-mail.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/1ebaa67)]

Lastly, Sam James added support for [PyPDF](https://pypi.org/project/pypdf/) version 3&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6aed2e53)] and Vagrant Cascadian updated a handful of tool references for [GNU Guix](https://guix.gnu.org/).&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a0b80552)][[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a10a169c)]


---

### Upstream patches

The Reproducible Builds project attempts to fix as many currently-unreproducible packages as possible. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`argparse`](https://github.com/praiskup/argparse-manpage/pull/76) (date-related issue)
    * [`asciimatics`](https://github.com/peterbrittain/asciimatics/pull/365) (build failure)
    * [`asyncpg`](https://github.com/MagicStack/asyncpg/issues/997) (fails to build in 2032)
    * [`cpython`](https://github.com/python/cpython/issues/101069) (fails to build in 2038)
    * [`django`](https://github.com/django/django/pull/16459) (fails to build in 2038)
    * [`grandorgue`](https://github.com/GrandOrgue/grandorgue/pull/1358) (`.zip`-related issue)
    * [`libarchive`](https://github.com/libarchive/libarchive/pull/1836) (fails to build in 2038)
    * [`libarchive`](https://github.com/libarchive/libarchive/pull/1838) (fails to build in 2038)
    * [`librcc`](https://github.com/RusXMMS/librcc/pull/5) (date)
    * [`mbedtls`](https://github.com/Mbed-TLS/mbedtls/issues/6978) (fails to build in 2023)
    * [`mozilla-nss`](https://bugzilla.mozilla.org/show_bug.cgi?id=1813401) (fails to build in 2023)
    * [`ocaml-rpm-macros`](https://build.opensuse.org/request/show/1061850) (fix fallout from an RPM-related change)
    * [`perl HTTP::Cookies`](https://github.com/libwww-perl/HTTP-Cookies/pull/72) (fails to build in 2038)
    * [`python-aiosmtplib/python-trustme`](https://build.opensuse.org/request/show/1058352) (fails to build in 2038 due to SSL certificate)
    * [`python-bmap`](https://github.com/intel/bmap-tools/issues/116) (fails to build in 2024)
    * [`python-compileall2`](https://github.com/fedora-python/compileall2/pull/26) (fails to build in 2038)
    * [`taskwarrior`](https://github.com/GothenburgBitFactory/taskwarrior/issues/3050) (`python-tasklib` fails to build in 2038)
    * [`taskwarrior`](https://github.com/GothenburgBitFactory/taskwarrior/pull/3052) (fix fails to build in 2038)
    * [`wrk`](https://github.com/wg/wrk/issues/507) (hash ordering issue)
    * [`xemacs`](https://build.opensuse.org/request/show/1058331) (fails to build in 2038 stuck)

* Chris Lamb:

    * [#1027988](https://bugs.debian.org/1027988) filed against [`click`](https://tracker.debian.org/pkg/click).
    * [#1027992](https://bugs.debian.org/1027992) filed against [`towncrier`](https://tracker.debian.org/pkg/towncrier).
    * [#1028051](https://bugs.debian.org/1028051) filed against [`unifrac-tools`](https://tracker.debian.org/pkg/unifrac-tools).
    * [#1028310](https://bugs.debian.org/1028310) filed against [`hamster-time-tracker`](https://tracker.debian.org/pkg/hamster-time-tracker).
    * [#1028515](https://bugs.debian.org/1028515) filed against [`accel-config`](https://tracker.debian.org/pkg/accel-config).
    * [#1029295](https://bugs.debian.org/1029295) filed against [`python-miio`](https://tracker.debian.org/pkg/python-miio).
    * [#1029297](https://bugs.debian.org/1029297) filed against [`python-graphviz`](https://tracker.debian.org/pkg/python-graphviz).

* Vagrant Cascadian:

    * [#1029227](https://bugs.debian.org/1029227) filed against [`ectrans`](https://tracker.debian.org/pkg/ectrans).
    * [#1029303](https://bugs.debian.org/1029303) filed against [`fiat-ecmwf`](https://tracker.debian.org/pkg/fiat-ecmwf).
    * [#1029307](https://bugs.debian.org/1029307) filed against [`node-katex`](https://tracker.debian.org/pkg/node-katex).
    * [#1029800](https://bugs.debian.org/1029800) filed against [`aribas`](https://tracker.debian.org/pkg/aribas).
    * [#1029801](https://bugs.debian.org/1029801) filed against [`tbox`](https://tracker.debian.org/pkg/tbox).
    * [#1029807](https://bugs.debian.org/1029807) filed against [`borgbackup2`](https://tracker.debian.org/pkg/borgbackup2).
    * [#1029809](https://bugs.debian.org/1029809) filed against [`dnf-plugins-core`](https://tracker.debian.org/pkg/dnf-plugins-core).
    * [#1030057](https://bugs.debian.org/1030057) filed against [`refpolicy`](https://tracker.debian.org/pkg/refpolicy).

* FC Stegerman:

    * Several patches for [`file(1)`](https://en.wikipedia.org/wiki/File_(command)) (which is used by reproducible builds tools like *diffoscope* and *strip-nondeterminism*) that improve detection of various file formats are now included in the Debian packaging.&nbsp;[[…](https://github.com/file/file/search?q=FC+Stegerman&type=commits)]

---

## Testing framework

[![]({{ "/images/reports/2023-01/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a comprehensive testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org) in order to check packages and other artifacts for reproducibility. In January, the following changes were made by Holger Levsen:

* Node changes:

    * Add three new nodes hosted at the [Oregon State University Open Source Lab](https://osuosl.org/) including integrating them into the DNS, maintenance and monitoring systems.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/48a18e51)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/26497180)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e151cc7c)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/93115d67)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/840dc8da)]

* Debian-related changes:

    * Only keep [diffoscope](https://diffoscope.org)'s HTML output (ie. no `.json` or `.txt`) for LTS suites and older in order to save diskspace on the Jenkins host.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/098742fb)]
    * Re-create `pbuilder` base less frequently for the `stretch`, `bookworm` and `experimental` suites.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/d2d7923c)]

* OpenWrt-related changes:

    * Add gcc-multilib to `OPENWRT_HOST_PACKAGES` and install it on the nodes that need it.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/7f88cba8)]
    * Detect more problems in the health check when failing to build OpenWrt.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/02ce1d17)]

* Misc changes:

    * Update the `chroot-run` script to correctly manage `/dev` and `/dev/pts`.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/0e49a9eb)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/a68ae0fd)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/f18ba089)]
    * Update the Jenkins 'shell monitor' script to collect disk stats less frequently&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/2ea61357)] and to include various directory stats.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/00400ad4)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e5b31286)]
    * Update the 'real' year in the configuration in order to be able to detect whether a node is running in the future or not.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/3827ba7c)]
    * Bump copyright years in the default page footer.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/ff82ec5f)]

In addition, Christian Marangi submitted a patch to build OpenWrt packages with the `V=s` flag to enable debugging.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e8db59c3)]

---

If you are interested in contributing to the Reproducible Builds project, please visit the [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. You can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mastodon: [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)

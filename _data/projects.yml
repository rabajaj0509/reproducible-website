- name: Alpine Linux
  url: https://alpinelinux.org/
  logo: alpine.png
  tests: https://tests.reproducible-builds.org/alpine/
  tests_disabled: true
- name: Apache Maven
  url: https://maven.apache.org/
  logo: apache-maven.png
  resources:
  - name: User Guide
    url: https://maven.apache.org/guides/mini/guide-reproducible-builds.html
  - name: Reproducible Central
    url: https://github.com/jvm-repo-rebuild/reproducible-central
- name: Arch Linux
  url: https://www.archlinux.org/
  logo: archlinux.png
  tests: https://tests.reproducible-builds.org/archlinux/
  resources:
  - name: Developer wiki page
    url: https://wiki.archlinux.org/index.php/DeveloperWiki:ReproducibleBuilds
  - name: Recording documentation
    url: /docs/recording/#arch-linux
  - name: verification tool for users
    url: https://github.com/archlinux/archlinux-repro
  - name: Rebuilds by Arch Linux
    url: https://reproducible.archlinux.org/
- name: Baserock
  url: https://baserock.org/
  logo: baserock.png
  resources:
  - name: Bit-for-bit reproducibility in Baserock systems
    url: https://wiki.baserock.org/projects/deterministic-builds/
- name: Bitcoin Core
  url: https://github.com/bitcoin/bitcoin
  logo: bitcoin.png
  resources:
  - name: Guix building guide
    url: https://github.com/bitcoin/bitcoin/blob/master/contrib/guix/README.md
- name: BitShares
  url: https://bitshares.org/
  logo: bitshares.png
  resources:
  - name: Gitian README
    url: https://github.com/bitshares/bitshares-gitian
- name: Buildroot
  url: https://buildroot.org/
  logo: buildroot.png
  resources:
  - name: Wiki page
    url: https://elinux.org/Buildroot:ReproducibleBuilds
- name: coreboot
  url: https://www.coreboot.org/
  logo: coreboot.png
  tests: https://tests.reproducible-builds.org/coreboot/
- name: Debian
  url: https://www.debian.org/
  logo: debian.png
  tests: https://tests.reproducible-builds.org/debian/
  resources:
  - name: Wiki pages
    url: https://wiki.debian.org/ReproducibleBuilds
  - name: Mailing list
    url: mailto:reproducible-builds@lists.alioth.debian.org
  - name: Recording
    url: /docs/recording/#debian
  - name: Docker Images (reproducible)
    url: https://debuerreotype.github.io/
  - name: Rebuilds by Frédéric Pierret
    url: https://beta.tests.reproducible-builds.org/debian.html
- name: ElectroBSD
  url: https://www.fabiankeil.de/gehacktes/electrobsd/
  logo: electrobsd.png
  resources:
  - name: Reproducibility status
    url: https://www.fabiankeil.de/gehacktes/electrobsd/#reproducible-electrobsd
- name: F-Droid
  url: https://www.f-droid.org/
  logo: f-droid.png
  resources:
  - name: Reproducible Builds documentation
    url: https://f-droid.org/docs/Reproducible_Builds
  - name: Verification Server Guide
    url: https://f-droid.org/docs/Verification_Server
  - name: f-droid.org verification server (Rebuilds)
    url: https://verification.f-droid.org/
- name: FreeBSD
  url: https://www.freebsd.org/
  logo: freebsd.png
  tests: https://tests.reproducible-builds.org/freebsd/
  resources:
  - name: Base system
    url: https://wiki.freebsd.org/ReproducibleBuilds
  - name: Ports
    url: https://wiki.freebsd.org/PortsReproducibleBuilds
  - name: Mailing list
    url: mailto:reproducibility@freebsd.org
- name: Freedesktop SDK
  url: https://freedesktop-sdk.io/
  logo: fdsdk.svg
- name: Fedora
  url: https://fedoraproject.org/wiki/Fedora_Project_Wiki
  logo: fedora.png
  tests: https://tests.reproducible-builds.org/fedora/
  tests_disabled: true
  resources:
  - name: Wiki pages
    url: https://fedoraproject.org/wiki/Reproducible_Builds
  - name: Helper scripts
    url: https://github.com/kholia/ReproducibleBuilds
- name: GNU Guix
  url: https://www.gnu.org/software/guix/
  logo: guix.png
  tests: https://data.guix.gnu.org/repository/1/branch/master/latest-processed-revision/package-reproducibility
  tests_external: true
  resources:
  - name: Build verification tool
    url: https://gnu.org/software/guix/manual/html_node/Invoking-guix-challenge.html
  - name: Guix features
    url: https://gnu.org/software/guix/manual/html_node/Features.html
  - name: Statement on reproducible builds
    url: https://www.gnu.org/software/guix/news/reproducible-builds-a-means-to-an-end.html
  - name: Package submission guidelines
    url: https://gnu.org/software/guix/manual/html_node/Submitting-Patches.html
- name: In-toto
  url: https://in-toto.io
  logo: in-toto.png
  resources:
  - name: In-toto APT transport repository
    url: https://github.com/in-toto/apt-transport-in-toto
  - name: In-toto Debian rebuilder setup
    url: https://salsa.debian.org/reproducible-builds/debian-rebuilder-setup
- name: MirageOS
  url: https://mirage.io/
  logo: mirageos-logo.svg
  resources:
  - name: Reproducible OPAM builds
    url: https://builds.robur.coop/
  - name: Deploying binary MirageOS unikernels
    url: https://hannes.robur.coop/Posts/Deploy
- name: Monero
  url: https://getmonero.org/
  logo: monero.png
  resources:
  - name: GitHub tracking issue
    url: https://github.com/monero-project/monero/issues/2641
- name: NetBSD
  url: https://www.netbsd.org/
  logo: netbsd.png
  tests: https://tests.reproducible-builds.org/netbsd/
  resources:
  - name: Notes on Reproducible Builds of NetBSD
    url: https://blog.netbsd.org/tnf/entry/netbsd_fully_reproducible_builds
- name: NixOS
  url: https://nixos.org/
  logo: nixos.png
  tests: https://reproducible.nixos.org
  tests_external: true
  resources:
  - name: Open issues
    url: https://github.com/NixOS/nixpkgs/issues?q=is%3Aopen+is%3Aissue+label%3A%226.topic%3A+reproducible+builds%22
  - name: Open PRs
    url: https://github.com/NixOS/nixpkgs/pulls?q=is%3Aopen+is%3Apr+label%3A%226.topic%3A+reproducible+builds%22
- name: OpenEmbedded
  url: https://www.openembedded.org/wiki/Main_Page
  logo: oe-logo.png
- name: openSUSE
  url: https://www.opensuse.org/
  logo: openSUSE.png
  tests: http://rb.zq1.de/compare.factory/
  tests_external: true
  resources:
  - name: openSUSE & Reproducible Builds
    url: https://en.opensuse.org/openSUSE:Reproducible_Builds
  - name: Overall blocking bug
    url: https://bugzilla.opensuse.org/show_bug.cgi?id=1133809
  - name: openSUSE r-b CI graph
    url: http://rb.zq1.de/compare.factory/graph.png
- name: OpenWrt
  url: https://openwrt.org/
  logo: openwrt.png
  tests: https://tests.reproducible-builds.org/openwrt/
  resources:
- name: openEuler
  url: https://www.openeuler.org/
  logo: openEuler.png
  tests: https://reproducible-builds.openeuler.org/
  tests_external: true
  resources:
- name: Qubes OS
  url: https://www.qubes-os.org/
  logo: qubes.png
  tests: https://qubesos.gitlab.io/qubes-g2g-report/
  tests_external: true
  resources:
  - name: General GitHub Issue
    url: https://github.com/QubesOS/qubes-issues/issues/816
  - name: Mailing lists
    url: https://www.qubes-os.org/mailing-lists/
  - name: Rebuilds by Frédéric Pierret
    url: https://beta.tests.reproducible-builds.org/qubesos.html
- name: SecureDrop
  url: https://securedrop.org
  logo: securedrop.png
  resources:
  - name: Package building repository
    url: https://github.com/freedomofpress/securedrop-debian-packaging/
  - name: Chat
    url: https://gitter.im/freedomofpress/securedrop
- name: Symfony
  url: https://symfony.com
  logo: symfony.png
  resources:
  - name: 'New in Symfony: Reproducible Builds'
    url: https://symfony.com/blog/new-in-symfony-reproducible-builds
- name: Tails
  url: https://tails.boum.org/
  logo: tails.png
  resources:
  - name: Verifying a Tails image for reproducibility
    url: https://tails.boum.org/contribute/build/reproducible/
  - name: Blueprint
    url: https://tails.boum.org/blueprint/reproducible_builds/
  - name: Ticket
    url: https://labs.riseup.net/code/issues/5630
  - name: Mailing list
    url: https://tails.boum.org/about/contact/index.en.html#index3h2
  - name: Recording
    url: /docs/recording/#tails
- name: Talos Linux
  url: https://www.talos.dev/
  logo: talos.svg
  resources:
  - name: Getting Started Guide
    url: https://www.talos.dev/v1.0/introduction/getting-started/
  - name: Source code repository
    url: https://github.com/siderolabs/talos/
  - name: Community Slack
    url: https://slack.dev.talos-systems.io/
  - name: Sidero Labs
    url: https://siderolabs.com/
- name: TREZOR
  url: https://trezor.io/
  logo: trezor.png
  resources:
  - name: Developer wiki page
    url: https://wiki.trezor.io/Developers_guide:Deterministic_firmware_build
- name: Tor Browser
  url: https://www.torproject.org/
  logo: tor.png
  resources:
  - name: Building guide
    url: https://gitlab.torproject.org/tpo/applications/tor-browser-build
  - name: Reproducible Builds Manager
    url: https://rbm.torproject.org/
- name: Webconverger
  url: https://webconverger.com/
  logo: webconverger.png
  resources:
  - name: Introducing Reproducible Builds
    url: https://www.youtube.com/watch?v=FaBFbkBhnXI
- name: Yocto Project
  url: https://www.yoctoproject.org/
  logo: yocto-project.png
  resources:
  - name: Reproducible builds overview
    url: https://wiki.yoctoproject.org/wiki/Reproducible_Builds
  - name: Mailing list
    url: https://lists.yoctoproject.org/listinfo/yocto
  - name: "'Binary Reproducibility' listed as a feature"
    url: https://www.yoctoproject.org/software-overview/features/
  tests: https://www.yoctoproject.org/reproducible-build-results/
  tests_external: true
- name: Trisquel GNU/Linux
  url: https://trisquel.info/
  logo: trisquel.png
  tests: https://gitlab.com/debdistutils/reproduce-trisquel/
  tests_external: true
  resources:
